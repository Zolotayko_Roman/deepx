﻿using MediatR;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DeepX.Features.Comments.Create
{
    [NotMapped]
    public class Command : IRequest<Guid>
    {
        public Guid Id { get; set; }

        public string CommentText { get; set; }

        public DateTime CreateDate { get; set; }
    }

}
