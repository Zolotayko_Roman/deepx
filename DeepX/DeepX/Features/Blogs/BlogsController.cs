﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Threading.Tasks;

namespace DeepX.Features.Blogs
{
    public class BlogsController : Controller
    {
        private readonly IMediator _mediator;

        public BlogsController(IMediator mediator) => _mediator = mediator;

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Create.Command command)
        {
            command.Photo = GetPhotoByte();

            await _mediator.Send(command);

            return View();
        }

        private byte[] GetPhotoByte()
        {
            byte[] bytePhoto = { };

            var file = Request.Form.Files["photo"];

            if (file.Length > 0)
            {
                using (var ms = new MemoryStream())
                {
                    file.CopyTo(ms);
                    bytePhoto = ms.ToArray();
                }
            }

            return bytePhoto;
        }
    }
}