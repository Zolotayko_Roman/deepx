﻿using DeepX.Models;
using MediatR;
using System;
using System.Collections.Generic;

namespace DeepX.Features.Blogs.Create
{
    public class Command : IRequest<Guid>
    {
        public Guid Id { get; set; }

        public string Author { get; set; }

        public string Description { get; set; }

        public byte[] Photo { get; set; }

        public DateTime CreateDate { get; set; }

        public ICollection<Comment> Comments { get; set; }
    }
}
