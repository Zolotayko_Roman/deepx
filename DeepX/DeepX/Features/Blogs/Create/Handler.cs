﻿using AutoMapper;
using DeepX.Models;
using DeepX.Models.ViewModels;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DeepX.Features.Blogs.Create
{
    public class Handler: IRequestHandler<Command,Guid>
    {
        private readonly DeepXContext _context;

        public Handler(DeepXContext context) => _context = context;

        async Task<Guid> IRequestHandler<Command, Guid>.Handle(Command request, CancellationToken cancellationToken)
        {
            _context.Blogs.Add(ToEntity(request));

            await _context.SaveChangesAsync();

            return request.Id;
        }

        private Blog ToEntity(Command model)
        {
            return new Blog
            {
                Author = model.Author,
                Description = model.Description,
                Photo = model.Photo,
                CreateDate = DateTime.UtcNow
            };
        }
    }
}
