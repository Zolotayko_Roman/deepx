﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using DeepX.Features.Comments.Create;
using DeepX.Models.ViewModels;

namespace DeepX.Models
{
    public class DeepXContext : IdentityDbContext<IdentityUser>
    {
        public DeepXContext (DbContextOptions<DeepXContext> options)
            : base(options)
        {
        }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<Blog> Blogs { get; set; }
    }
}
