﻿using DeepX.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeepX.Seeds
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new DeepXContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<DeepXContext>>()))
            {
                // Look for any movies.
                if (context.Comments.Any())
                {
                    return;   // DB has been seeded
                }

                context.Comments.AddRange(
                    new Comment
                    {
                        Author = ".Net Jesus",
                        Message = "No comments.",
                        CreateDate = DateTime.UtcNow
                    },

                    new Comment
                    {
                        Author = "Slava KPSS",
                        Message = "BattleRap is alive.",
                        CreateDate = DateTime.UtcNow
                    },

                    new Comment
                    {
                        Author = "Man in black",
                        Message = "Get down.",
                        CreateDate = DateTime.UtcNow
                    }
                );
                context.SaveChanges();
            }
        }
    }
}
