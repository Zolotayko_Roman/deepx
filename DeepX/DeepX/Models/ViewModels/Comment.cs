﻿using DeepX.Models.ViewModels;

namespace DeepX.Models
{
    public class Comment : BaseViewModel
    {
        public string Message { get; set; }

        public Blog Blog { get; set; }
    }
}
