﻿using System.Collections.Generic;

namespace DeepX.Models.ViewModels
{
    public class Blog : BaseViewModel
    {
        public string Description { get; set; }

        public byte[] Photo { get; set; }

        public ICollection<Comment> Comments;
    }
}
